This tool can be used to add friends to your Tox profile using only public key, instead of whole Tox ID.

Note that **friend request will not be sent**.

Utility requires newest [**toxcore**](https://github.com/irungentoo/toxcore) installed. If you don't have it yet, you should follow instructions in its [INSTALL.md](https://github.com/irungentoo/toxcore/blob/master/INSTALL.md).

When you will have toxcore installed, you can easily compile this tool:

1. Install [Rust](http://www.rust-lang.org/)
2. Make with `cargo build`
3. Run with `./target/debug/./tox-add-pubkey <PUBLIC KEY> <.tox PROFILE>`

Instructions were tested only under Linux.

# License

Licensed under GPLv3+, for details see [COPYING](/COPYING).
