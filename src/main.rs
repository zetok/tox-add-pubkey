/*
    Copyright © 2015-2016 Zetok Zalbavar <zetok@openmailbox.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



/*
    Binding to toxcore
*/
extern crate rstox;
use rstox::core::*;
use rstox::core::errors::FriendAddError;

use std::env::args;

use std::io::prelude::*;
use std::fs::File;

use std::process::exit;

use std::str::FromStr;

/**
    Function to load profile.

    In case where it can't be opened, return an error, so that it could
    be printed.
*/
fn load_save(f: &str) -> Result<Vec<u8>, String> {
    match File::open(f) {
        Ok(mut file) => {
            let mut res: Vec<u8> = Vec::new();
            drop(file.read_to_end(&mut res));
            Ok(res)
        },

        Err(e) => {
            Err(format!("{}", e))
        },
    }
}
/**
    Function to write save file to storage.

    In case where it can't be written to, return an error, so that it could
    be printed.
*/
fn write_save(f: &str, data: Vec<u8>) -> Result<(), String> {
    match File::create(f) {
        Ok(mut file) => {
            drop(file.set_len(data.len() as u64));
            drop(file.write(&data));
            Ok(())
        },

        Err(e) => {
            Err(format!("{}", e))
        },
    }
}

/**
    Function to print a help message.
*/
fn help() {
    let name = args().next().unwrap();
    println!("  {} [-h] <Public Key> <path to Tox file>\n", name);
    println!("{} is a utility for adding to Tox files public keys without NoSpam and checksum.\n", name);
    println!("Public Key is 64 characters long string.");
    println!("\n  -h\tPrint this help message.");
    println!("\nWARNING: No data loss isn't guaranteed!
Consider yourself warned.");
}

/**
    Verify args.
    If args are invalid, print an error message and exit.
*/
fn verify_args(args: &Vec<String>) {
    match args.len() {
        3 => {},
        2 => {
            match &*args[1] {
                "-h" | "--help" => {
                    help();
                    exit(0);
                }
                _ => {
                    println!("Wrong argument!\n");
                    help();
                    exit(1);
                }
            }
        },
        _ => {
            println!("Wrong number of arguments!\n");
            help();
            exit(1);
        }
    }
}

fn arg_to_pk(args: &Vec<String>) -> PublicKey {
    // print an error and exit early if arg is not a pubkey
    fn not_a_pk() -> ! {
        println!("Supplied string is not a public key!\n");
        help();
        exit(1);
    }

    // 2nd arg is the pubkey
    let arg = &args[1];

    match arg.len() {
        76 => {
            if let Ok(id) = Address::from_str(arg) {
                println!("{} is an ID, not a PK, continuing anyway.\n", arg);
                return id.public_key().clone();
            }
        },
        64 => {
            if let Ok(pk) = PublicKey::from_str(arg) {
                return pk;
            }
        },
        _ => {},
    }
    // if `match` didn't return early, string couldn't be parsed as PK
    not_a_pk();
}

/// Print errors and exit
fn print_errs(err: FriendAddError) {
    // print a message about no changes made to the profile and exit
    // with a proper exit code
    fn exit_msg(bad_end: bool) {
        println!("\nEnding without any changes to the profile.");

        // `exit(1)` is for special, "extreme" errors that shouldn't have happend
        if bad_end { exit(1); } else { exit(0); }
    }

    let mut bad_end = false;
    match err {
        FriendAddError::NullError => {
            println!("An error occured that should not have happened. Please make a bug report about it.");
            println!("ERR: FriendAddError::NullError");
            bad_end = true;
        },
        FriendAddError::TooLong => println!("Public Key is too long."),
        FriendAddError::NoMessage => println!("No message was supplied."),
        FriendAddError::OwnKey => println!("Supplied public key matches own key."),
        FriendAddError::AlreadySent => println!("This public key is already added."),
        FriendAddError::BadChecksum => {
            println!("An error occured that should not have happened. Please make a bug report about it.");
            println!("ERR: FriendAddError::BadChecksum");
            bad_end = true;
        },
        FriendAddError::SetNewNospam => println!("Supplied key had a new nospam (!!)."),
        FriendAddError::MallocError => {
            println!("Problem with allocating memory when adding key.");
            bad_end = true;
        },
        FriendAddError::NoError => println!("No error (!!)."),
    }
    // ↓ nevar forget to exit
    exit_msg(bad_end);
}


fn main() {
    let argsv: Vec<String> = args().collect();
    verify_args(&argsv);

    let pk = arg_to_pk(&argsv);
    let file = &argsv[2];

    let data = match load_save(file) {
        Ok(d) => Some(d),
        Err(e) => {
            println!("Error while loading profile: {}", e);
            exit(1);
        },
    };
    let mut tox = Tox::new(ToxOptions::new(), data.as_ref()
                                            .map(|x| &**x)).unwrap();

    match tox.add_friend_norequest(&pk) {
        Ok(()) => println!("Successfully added {}.", &argsv[1]),
        Err(e) => print_errs(e),
    }

    match write_save(file, tox.save()) {
        Ok(_) => println!("{} was saved successfully.", file),
        Err(e) => println!("Failed to save {}:\n{}", file, e),
    }
}
